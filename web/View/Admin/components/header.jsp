<%-- 
    Document   : header
    Created on : May 9, 2024, 8:56:56 PM
    Author     : ACERR
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ADMIN</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" type="text/css" media="screen" href="../css/admin.css" />
        <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css"
            integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A=="
            crossorigin="anonymous"
            referrerpolicy="no-referrer"
            />
    </head>
    <body>
        <div class="header">
            <div class="header__left">
                <div class="dashboard">
                    <a href="#" class="dashboard__text">Dashboard</a>
                </div>
                <div class="box-search">
                    <i class="fa-solid fa-magnifying-glass"></i>
                    <input type="text" placeholder="Search..." />
                </div>
            </div>
            <div class="header__right">
                <div class="box-profile">
                    <div class="box-profile__white">
                        <img class="box-profile__image" src="../../../Image/profile-pic.png" alt="Not found" />
                        <div class="box-profile__text-right">
                            <p class="box-profile__name">Name:</p>
                            <p class="box-profile__role">Role:</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="Admin.js"></script>
    </body>
</html>
